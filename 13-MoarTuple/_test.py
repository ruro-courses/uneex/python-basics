from pytest_common import imp, pytest


@pytest.mark.parametrize(
    "a, b, n, value",
    [
        ((25, 0, -115, 976, 100500, 7), (32, 5, 78, 98, 10, 9, 42), 5, True),
        ([25] * 100500, [25, 26] * (100500 * 3 // 4), 5, True),
        (range(10), range(20, 30), 7, False),
    ]
)
def test_moartuple(a, b, n, value):
    moar = imp(__file__, 'solution.py', items='moar')
    result = moar(a, b, n)
    assert result == value
