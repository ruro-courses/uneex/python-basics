### Подсчёт кратных

Написать функцию moar(a, b, n) от трёх параметров --- целочисленных
кортежей a и b, и натурального числа n. Функция возвращает True, если в
a больше чисел, кратных n, чем в b, и False в противном случае.

### Examples

#### Input

    moar((25,0,-115,976,100500,7),(32,5,78,98,10,9,42),5)

#### Output

    True

### Submit a solution
