def moar(a, b, n):
    def divisible(v):
        return not v % n

    return sum(map(divisible, a)) > sum(map(divisible, b))
