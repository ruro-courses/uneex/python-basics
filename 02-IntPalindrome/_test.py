from pytest_common import run, pytest


@pytest.mark.parametrize(
    "number, is_palindrome",
    [
        # Basics
        (12321, True),
        (123321, True),
        (1232, False),
        (12332, False),
        (123214, False),
        (1233214, False),

        # With endline/whitespace
        ("12321\n", True),
        ("123321\n", True),
        ("1232\n", False),
        ("12332\n", False),
        ("  123214\n", False),
        ("1233214\r\n", False),

        # Zero edge cases
        (123210, False),
        (12021, True),
        (0, True),
        ("00012321", True)
    ]
)
def test_palindrome(number, is_palindrome):
    is_palindrome = "YES\n" if is_palindrome else "NO\n"
    result = run(__file__, 'solution.py', input=str(number))
    assert result.returncode == 0, result.stderr
    assert result.stdout == is_palindrome
