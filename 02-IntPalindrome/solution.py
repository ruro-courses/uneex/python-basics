def palindrome_check(num, other):
    if not num:
        return True, other

    rec_check, other = palindrome_check(num // 10, other)
    return rec_check and (num % 10 == other % 10), other // 10


def is_palindrome(num):
    if num < 0:
        return False

    return palindrome_check(num, num)[0]


print("YES" if is_palindrome(int(input())) else "NO")
