import operator

from pytest_common import imp, pytest

morse_ops = {
    '-': operator.neg,
    '+': operator.pos,
    '~': operator.inv,
}
c = '--+~-~-++~+++-'
d = '++-~-+-+~-'


@pytest.mark.parametrize(
    "op_string, arg, result",
    [
        ('-+', None, 'dah dit.'),
        ('-++~+-+', None, 'dah di dit, di dah dit.'),
        (c, '.-', '--. - -.. ...-'),
        (c, '..-', '--. - -.. ...-'),
        (c, '..-|', '--. - -.. ...-|'),
        [
            c, 'dot DOT dash',
            'dash dash DOT, dash, dash dot DOT, dot dot dot dash.'
        ],
        [
            c, 'ai aui oi ',
            'oi oi aui, oi, oi ai aui, ai ai ai oi'
        ],
        [
            c, 'dot dot dash ///',
            'dash dash dot, dash, dash dot dot, dot dot dot dash///'
        ],
        ('', None, '.'),
        ('+-+-', 'ZZ', 'ZZZZ'),
        ('+-+-', 'ZZZZ', 'ZZZZZ'),
        (d, 'Boo Boo Boo Boo', 'Boo Boo Boo, Boo Boo Boo Boo, BooBoo'),
        (d, 'Boo Boo Boo ', 'Boo Boo Boo, Boo Boo Boo Boo, Boo'),
        (d, 'Boo Boo Boo', 'Boo Boo Boo, Boo Boo Boo Boo, Boo.'),
    ]
)
def test_morse(op_string, arg, result):
    morse = imp(__file__, 'solution.py', items='morse')
    args = [] if arg is None else [arg]
    obj = morse(*args)
    for op in op_string[::-1]:
        obj = morse_ops[op](obj)
    assert str(obj) == result
