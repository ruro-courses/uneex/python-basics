### Склько экземпляров

Написать класс WeAre, объекты которого содержат поле count, содержащее
количество существующих экземпляров этого класса. Игнорировать попытки
изменить значение этого поля вручную или удалить его.

### Examples

#### Input

    a = WeAre()
    print(a.count)
    b, c = WeAre(), WeAre(),
    a.count = 100500
    print(a.count, b.count, c.count)
    del b.count
    del b
    print(a.count)

#### Output

    1
    3 3 3
    2

### Submit a solution
