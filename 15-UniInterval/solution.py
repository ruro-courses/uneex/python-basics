from collections import namedtuple
Segment = namedtuple("Segment", ("left", "right"))


def extend_right(seg_left, seg_right):
    return (
        Segment(seg_left.left, seg_right.right)
        if seg_left.right < seg_right.right
        else seg_left
    )


total_length = 0
segments = sorted(map(Segment._make, eval(input())))

curr_seg = Segment(0, 0)
for next_seg in segments:
    if next_seg.left <= curr_seg.right:
        # Next segment intersects with the current segment
        # Extend the current segment to the right
        curr_seg = extend_right(curr_seg, next_seg)
    else:
        # No intersection with current segment
        # Add length for current segment and start growing a new segment
        total_length += curr_seg.right - curr_seg.left
        curr_seg = next_seg
total_length += curr_seg.right - curr_seg.left

print(total_length)
