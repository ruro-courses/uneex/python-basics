import collections.abc


def sizer(cls):
    __orig_getattr__ = getattr(cls, '__getattr__', None)

    def __getattr__(self, item):
        if item == 'size':
            if isinstance(self, collections.abc.Sized):
                return len(self)
            else:
                return abs(int(self))
        elif __orig_getattr__:
            return __orig_getattr__(self, item)
        else:
            return getattr(super(), item)

    cls.__getattr__ = __getattr__
    return cls
