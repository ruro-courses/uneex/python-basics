from pytest_common import imp


class S(str):
    pass


class N(float):
    pass


class L(list):
    pass


class C:
    def __len__(self):
        return 42


def test_sizer():
    sizer = imp(__file__, 'solution.py', items='sizer')
    _cls_s = sizer(S)
    _cls_n = sizer(N)
    _cls_c = sizer(C)
    _cls_l = sizer(L)
    s = _cls_s("QSXWDC")
    p = _cls_s("QSXWDC")
    c = _cls_c()
    n = _cls_n(2.718281828459045)
    m = _cls_l("QaZWsxEdC")
    assert str(s) == 'QSXWDC'
    assert str(n) == '2.718281828459045'
    assert str(m) == "['Q', 'a', 'Z', 'W', 's', 'x', 'E', 'd', 'C']"
    assert s.size == 6
    assert n.size == 2
    assert c.size == 42
    assert m.size == 9
    s.size = "Wait"
    n.size = "what?"
    p.size = "wrong"
    c.size = "custom"
    del m[2:5]
    assert s.size == "Wait"
    assert n.size == "what?"
    assert p.size == "wrong"
    assert c.size == "custom"
    assert m.size == 6
    assert str(m) == "['Q', 'a', 'x', 'E', 'd', 'C']"
