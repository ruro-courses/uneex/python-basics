from pytest_common import imp, pytest


@pytest.mark.parametrize(
    "lefts, rights, values",
    [

        list(zip(*[
            (1, 2, 1),
            (1, 2, 2),
            (1, 2, 1),
            (3, 4, 4),
            (3, 4, 3)
        ])),
        list(zip(*[
            (5, 7, 5),
            (5, 7, 7),
            (5, 7, 5),
            (5, 17, 17),
            (5, 17, 5),
            (5, 17, 17),
            (5, 17, 5),
            (5, 17, 17),
        ]))
    ]
)
def test_randint(lefts, rights, values):
    randint = imp(__file__, 'solution.py', items='randint')
    results = [randint(left, right) for left, right in zip(lefts, rights)]
    assert results == list(values)


@pytest.mark.parametrize(
    "args, values",
    [
        (
                [
                    (5, 6),
                    (10, 15, 2, 7),
                    (20, 36, 3, False, 9)
                ],
                [
                    0, 1, 2, 3, 4, 0,
                    10, 12, 14, 11, 13, 10, 12,
                    20, 23, 26, 29, 32, 35, 22, 25, 28
                ]
        ),
        (
                [
                    (3, 0, -1, 6),
                    (3, 0, -2, 6)
                ],
                [
                    3, 2, 1, 3, 2, 1,
                    3, 1, 2, 3, 1, 2
                ]
        )
    ]
)
def test_randrange(args, values):
    randrange = imp(__file__, 'solution.py', items='randrange')
    result = [
        randrange(*arg)
        for *arg, times in args
        for _ in range(times)
    ]
    assert result == values
