def randint(a, b):
    randint.state = not randint.state
    return a if randint.state else b


def randrange(start, stop=None, step=1, _int=None):
    import itertools

    del _int
    if (start, stop, step) != randrange.state:
        value_range = itertools.cycle(
            range(start)
            if stop is None
            else range(start, stop, (step > 0) - (step < 0))
        )
        randrange.iter = itertools.islice(value_range, None, None, abs(step))
        randrange.state = (start, stop, step)

    return next(randrange.iter)


randrange.iter = iter(())
randrange.state = None
randint.state = None
