def xywh_to_p1p2(x, y, w, h):
    x1, y1, x2, y2 = x, y, x + w, y + h
    x1, x2 = sorted((x1, x2))
    y1, y2 = sorted((y1, y2))
    return (x1, y1), (x2, y2)


def parse_rect(line):
    *coords, c = line.split()
    x, y, w, h = map(int, coords)
    if (not w) or (not h):
        return None
    return xywh_to_p1p2(x, y, w, h), c


def abs_to_rel(rect, off):
    ((px1, py1), (px2, py2)), c = rect
    xo, yo = off
    return ((px1 - xo, py1 - yo), (px2 - xo, py2 - yo)), c


def get_extents(points):
    xmin, xmax = min(x for x, _ in points), max(x for x, _ in points)
    ymin, ymax = min(y for _, y in points), max(y for _, y in points)
    return (xmin, ymin), (xmax - xmin, ymax - ymin)


def set_field(field, rect):
    ((px1, py1), (px2, py2)), c = rect
    replacement = (px2 - px1) * c
    for i in range(py1, py2):
        field[i] = field[i][:px1] + replacement + field[i][px2:]
    return field


def main():
    rects = list(
        filter(
            lambda r: r is not None,
            map(parse_rect, iter(input, "0 0 0 0 0"))
        )
    )
    points = [p for r, _ in rects for p in r]
    offset, size = get_extents(points)
    rects = [abs_to_rel(r, offset) for r in rects]
    field = ['.' * size[0]] * size[1]
    for r in rects:
        field = set_field(field, r)
    print('\n'.join(''.join(row) for row in field))


main()
