from pytest_common import run, pytest
from math import isclose, pi


@pytest.mark.parametrize(
    "function, left, right, solution",
    [
        ("sin(x+0.00007)", -1, 2, -7e-05),
        ("x**2 - 2", 0, 4, 2**0.5),
        ("log2(x - pi)", 4, 5, 1 + pi),
        ("sin(x)", 3.5902069883288776, 7.635721280269889, 2*pi),
        (
            "x+exp(x)-x**2/2",
            -1.8999828537179801, 2.192476327785152,
            -0.4912251835444739
        ),
        (
            "cos(x**2)/2-x/7",
            -1.9373766022064174, -0.7141707854785669,
            -1.4089359709016319
        ),
        (
            "x**5-5*x**3-3*x**2-2*x+5",
            -2.705025133979186, 0.4795595788236264,
            -2.1327169363239725
        )
    ]
)
def test_bisection(function, left, right, solution):
    result = run(__file__, 'solution.py', input="{}\n{}, {}".format(
        function, left, right
    ))
    assert result.returncode == 0, result.stderr
    assert isclose(eval(result.stdout), solution, rel_tol=0, abs_tol=1e-6)
