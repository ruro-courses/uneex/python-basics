import importlib.util
import os
import subprocess
import sys

# noinspection PyUnresolvedReferences
import pytest


def imp(*fname, items):
    fpath = os.path.join(*(
        os.path.dirname(subdir)
        for subdir in fname[:-1]
    ), fname[-1])
    fname = fname[-1]
    mname = fname.rsplit('.', 1)[0]

    assert os.path.exists(fpath)
    assert os.path.isfile(fpath)

    spec = importlib.util.spec_from_file_location(mname, fpath)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)

    if isinstance(items, str):
        return getattr(module, items)
    else:
        return (getattr(module, item) for item in items)


def run(*fname, **kwargs):
    kwargs.setdefault('universal_newlines', True)
    kwargs.setdefault('stdout', subprocess.PIPE)
    kwargs.setdefault('stderr', subprocess.PIPE)

    fname = os.path.relpath(os.path.join(*(
        os.path.dirname(subdir)
        for subdir in fname[:-1]
    ), fname[-1]))
    assert os.path.exists(fname)
    assert os.path.isfile(fname)
    return subprocess.run([sys.executable, fname], **kwargs)
