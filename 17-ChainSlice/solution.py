def chainslice(begin, end, *seqs):
    from itertools import chain, islice
    yield from islice(chain.from_iterable(seqs), begin, end)
