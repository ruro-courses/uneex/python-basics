def turtle(coord, direction):
    dirs = ((1, 0), (0, 1), (-1, 0), (0, -1))

    def move(_coord, _direction):
        cx, cy = _coord
        dx, dy = dirs[_direction % 4]
        return cx + dx, cy + dy

    while True:
        cmd = yield coord
        coord, direction = {
            'l': lambda c, d: (c, d + 1),
            'r': lambda c, d: (c, d - 1),
            'f': lambda c, d: (move(c, d), d),
        }[cmd](coord, direction)
