def nonify(f):
    from functools import wraps
    @wraps(f)
    def _f(*args, **kwargs):
        return f(*args, **kwargs) or None

    return _f
