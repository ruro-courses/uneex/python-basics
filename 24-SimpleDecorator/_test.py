from pytest_common import imp, pytest


def aNb(a, n, b):
    return a * n + b


def both(a, b, c=100500, d="QQ"):
    return a != c and b != d


@pytest.mark.parametrize(
    "fun, args, kwargs, value",
    [
        (aNb, [1, 2, 3], {}, 5),
        (aNb, [], {'a': 1, 'n': 2, 'b': 3}, 5),
        (aNb, ["QWE", 0, ""], {}, None),
        (aNb, [], {'a': "QWE", 'n': 0, 'b': ""}, None),
        (both, [1, 2, 3, 4], {}, True),
        (both, [1, 2], {'d': 3}, True),
        (both, [100500, 7], {'d': 7}, None),
    ]
)
def test_simpledecorator(fun, args, kwargs, value):
    nonify = imp(__file__, 'solution.py', items='nonify')
    result = nonify(fun)(*args, **kwargs)
    assert result == value
