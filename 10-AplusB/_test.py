from pytest_common import imp, pytest


@pytest.mark.parametrize(
    "left, right, value",
    [
        (1, 2, 6),
        (3, 4, 14),
        (14, 5, 38),
        ("", "", ""),
        ("A", "B", "ABAB"),
    ]
)
def test_aplusb2(left, right, value):
    AplusB2 = imp(__file__, 'solution.py', items='AplusB2')
    result = AplusB2(left, right)
    assert result == value
