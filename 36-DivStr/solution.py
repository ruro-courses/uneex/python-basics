import functools


def _as_div_str(meth):
    @functools.wraps(meth)
    def wrap(*args, **kwargs):
        val = meth(*args, **kwargs)
        if isinstance(val, str):
            val = DivStr(val)
        return val

    return wrap


class DivStr(str):
    def __truediv__(self, other):
        return self[:len(self) // other]

    __add__ = _as_div_str(str.__add__)
    __format__ = _as_div_str(str.__format__)
    __getitem__ = _as_div_str(str.__getitem__)
    __mod__ = _as_div_str(str.__mod__)
    __mul__ = _as_div_str(str.__mul__)
    __repr__ = _as_div_str(str.__repr__)
    __rmul__ = _as_div_str(str.__rmul__)
    __str__ = _as_div_str(str.__str__)
    capitalize = _as_div_str(str.capitalize)
    casefold = _as_div_str(str.casefold)
    center = _as_div_str(str.center)
    expandtabs = _as_div_str(str.expandtabs)
    format = _as_div_str(str.format)
    format_map = _as_div_str(str.format_map)
    join = _as_div_str(str.join)
    ljust = _as_div_str(str.ljust)
    lower = _as_div_str(str.lower)
    lstrip = _as_div_str(str.lstrip)
    replace = _as_div_str(str.replace)
    rjust = _as_div_str(str.rjust)
    rstrip = _as_div_str(str.rstrip)
    strip = _as_div_str(str.strip)
    swapcase = _as_div_str(str.swapcase)
    title = _as_div_str(str.title)
    translate = _as_div_str(str.translate)
    upper = _as_div_str(str.upper)
    zfill = _as_div_str(str.zfill)
