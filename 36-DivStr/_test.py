from pytest_common import imp


class C:
    def __str__(self):
        return "I am absolutely nobody"


def test_divstr():
    _cls_div_str = imp(__file__, 'solution.py', items='DivStr')
    s = _cls_div_str("Qwertupy")
    assert len(s) == 8
    assert s * 3 / 2 == 'QwertupyQwer'
    assert (s + "ZZZZZZZZZZ") / 2 == 'QwertupyZ'
    assert s[3:] / 2 == 'rt'
    assert s[0].join("12345678") / 2 == '1Q2Q3Q4'
    assert _cls_div_str(100500 ** 4) / 3 == '1020150'
    assert _cls_div_str(dir) / 3 == '<built-'
    assert _cls_div_str(C()) / 2 == 'I am absolu'
    s = _cls_div_str("qazxwie fhqc ioerthweqriothgweiortyweoir ")
    assert s.upper() / 4 == 'QAZXWIE FH'
    assert s.title() / 2 == 'Qazxwie Fhqc Ioerthw'
    assert (
            (s.title() / 6).center(40, "*") * 3 / 4
            == '*****************Qazxwi*******'
    )
    assert _cls_div_str("100500").isnumeric()
    assert s.replace(_cls_div_str("zx"), "TH") / 5 == 'qaTHwie '
