from pytest_common import imp, pytest


def f1(a):
    return a + 1


def f2(a, b, _f1):
    return _f1(a) + _f1(b)


def compare(a, b):
    return a > b


def write(seq, i, val):
    seq[i] = val


def read(seq, i):
    return seq[i]


def bubblesort(seq, _read, _write, _compare):
    for i in range(len(seq) - 1):
        for j in range(len(seq) - 1 - i):
            a, b = _read(seq, j), _read(seq, j + 1)
            if _compare(a, b):
                _write(seq, j, b)
                _write(seq, j + 1, a)


def qsort(seq, i, j, _qsort, _read, _write, _compare):
    if j - i < 2:
        if i == j:
            return
        a, b = _read(seq, i), _read(seq, j)
        if _compare(a, b):
            _write(seq, i, b)
            _write(seq, j, a)
    else:
        ni, nj, d = i, j, (1, 0)
        while ni != nj:
            a, b = _read(seq, ni), _read(seq, nj)
            if _compare(a, b):
                _write(seq, ni, b)
                _write(seq, nj, a)
                d = 1 - d[0], -1 - d[1]
            ni += d[0]
            nj += d[1]
        _qsort(seq, i, ni - 1, _qsort, _read, _write, _compare)
        _qsort(seq, ni, j, _qsort, _read, _write, _compare)


@pytest.mark.parametrize(
    "funs, expression, value, stats",
    [
        (
                [f1, f2],
                lambda _f1, _f2: _f1(_f2(2, 3, _f1) + _f2(5, 6, _f1)),
                21, {f1: 5, f2: 2}
        ),
        (
                [read, write, compare],
                lambda **fs: bubblesort(
                    [
                        (a * b + 4) % 17 + b % 3 + a // 10
                        for a in range(20) for b in range(20)
                    ], **fs
                ), None, {compare: 79800, read: 159600, write: 70418}
        ),
        (
                [read, write, compare, qsort],
                lambda **fs: fs['_qsort'](
                    [
                        (a ^ b * 2 + a * 3 + b * 13 + 3) % 40
                        for a in range(20) for b in range(20)
                    ], 0, 399, **fs
                ), None, {compare: 5070, read: 10140, write: 1732, qsort: 669}
        ),
    ]
)
def test_statcounter(funs, expression, value, stats):
    statcounter = imp(__file__, 'solution.py', items='statcounter')
    sc = statcounter()
    result_stats = next(sc)
    wrapped_funs = {'_' + f.__name__: sc.send(f) for f in funs}

    for f_name, wf in wrapped_funs.items():
        assert f_name == '_' + wf.__name__

    assert expression(**wrapped_funs) == value
    assert result_stats == stats
