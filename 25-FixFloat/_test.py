from math import cos, pi, sin

from pytest_common import imp, pytest


def aver(*args, sign=1):
    return sum(args) * sign


def sicos(a, b):
    return sin(a) ** 2 + cos(b) ** 2


def from_digits(digits):
    return float(f'0.{digits}')


def to_string(value):
    return str(value)


@pytest.mark.parametrize(
    "fun, ndigits, args, kwargs, value",
    [
        (
                aver, 4,
                [2.45675901, 3.22656321, 3.432654345, 4.075463224],
                {'sign': -1},
                -13.1916
        ),
        (
                sicos, 3,
                [pi / 2, pi / 2], {},
                1.0
        ),
        (
                sicos, 3,
                [pi / 4, 3 * pi / 4], {},
                0.999
        ),
        (
                from_digits, 2,
                ['1234'], {},
                0.12
        ),
        (
                to_string, 2,
                [0.1234], {},
                '0.12'
        )
    ]
)
def test_fixfloat(fun, ndigits, args, kwargs, value):
    fix = imp(__file__, 'solution.py', items='fix')
    result = fix(ndigits)(fun)(*args, **kwargs)
    assert result == value
