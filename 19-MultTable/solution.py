style = {
    'prod': ' * ',
    'aseq': ' = ',
    'vsep': ' | ',
    'hsep': '=',
}
FSTRING = "{i:>{mult_len}}{prod}{j:<{mult_len}}{aseq}{p:<{prod_len}}"


def zip_from_iter(xs):
    iters = [iter(x) for x in xs]
    try:
        while True:
            yield tuple([next(it) for it in iters])
    except StopIteration:
        return


def get_mult_data(max_mult, batch=1):
    table_rows = []
    current_row = []
    for i in range(1, 1 + max_mult):
        current_row.append([(i, j, i * j) for j in range(1, 1 + max_mult)])
        if not i % batch or i == max_mult:
            table_rows.append(list(zip_from_iter(current_row)))
            current_row = []
    return table_rows


def get_pagination(max_mult, max_width):
    max_mult_len = len(str(max_mult))
    max_prod_len = len(str(max_mult * max_mult))
    exp_symb_len = len(style['prod'] + style['aseq'])
    col_vsep_len = len(style['vsep'])
    exp_full_len = 2 * max_mult_len + max_prod_len + exp_symb_len
    if exp_full_len > max_width:
        raise RuntimeError(
            "Computed column width exceeds maximum allowed width"
        )
    columns = 1 + (max_width - exp_full_len) // (exp_full_len + col_vsep_len)
    return {
        **style,
        'header': style['hsep'] * max_width,
        'columns': columns,
        'mult_len': max_mult_len,
        'prod_len': max_prod_len
    }


def main():
    N, M = eval(input())
    pagination = get_pagination(N, M)
    mult_data = get_mult_data(N, pagination['columns'])
    print(pagination['header'])
    for table_row in mult_data:
        for column in table_row:
            print(
                pagination['vsep'].join(
                    FSTRING.format(i=i, j=j, p=p, **pagination)
                    for i, j, p in column
                )
            )
        print(pagination['header'])


main()
